package com.example.android.mvvmrepositorypattern.app;

import android.content.Context;

import com.example.android.mvvmrepositorypattern.data.local.UserDao;
import com.example.android.mvvmrepositorypattern.data.local.UserEntity;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = {UserEntity.class}, version = 2)
public abstract class AppDatabase extends RoomDatabase {

    private static AppDatabase instance;

    public abstract UserDao userDao();

    public static synchronized AppDatabase getDatabaseInstance(Context context) {

        if (instance == null) {
            instance = Room
                    .databaseBuilder(
                            context.getApplicationContext(),
                            AppDatabase.class,
                            "user_database")
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return instance;
    }
}
