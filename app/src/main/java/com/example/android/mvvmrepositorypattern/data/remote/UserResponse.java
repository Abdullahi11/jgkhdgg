package com.example.android.mvvmrepositorypattern.data.remote;

import com.google.gson.annotations.SerializedName;

public class UserResponse {

    @SerializedName("id")
    private int userId;

    @SerializedName("login")
    private String username;

    /**
    Constructors
     */
    public UserResponse() {
    }

    public UserResponse(int userId, String username) {
        this.userId = userId;
        this.username = username;
    }

    /**
    Getters and Setters
     */
    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
