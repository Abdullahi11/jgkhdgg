package com.example.android.mvvmrepositorypattern.ui;

import android.content.Context;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.example.android.mvvmrepositorypattern.R;
import com.example.android.mvvmrepositorypattern.app.AppDatabase;
import com.example.android.mvvmrepositorypattern.data.local.UserEntity;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class MainActivity extends AppCompatActivity {

    RecyclerView rvUser;
    UserAdapter adapter = new UserAdapter();
    UserViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initRecyclerView();
        initViewModel();
        fetchUsers();
        observeUsers();
    }

    private void initRecyclerView() {
        rvUser = findViewById(R.id.user_recycler_view);
        rvUser.setLayoutManager(new LinearLayoutManager(this));
        rvUser.setHasFixedSize(true);
        rvUser.setAdapter(adapter);
    }

    private void initViewModel() {
        viewModel = ViewModelProviders.of(this).get(UserViewModel.class);
    }

    private void fetchUsers() {
        if (isNetworkAvailable()) {
            viewModel.fetchDataFromNetworkAndStore();
        } else {
            Toast.makeText(this, "No internet, displayed cache data", Toast.LENGTH_SHORT)
                    .show();
        }
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }

    private void observeUsers() {
        viewModel.getUsers().observe(this, new Observer<List<UserEntity>>() {
            @Override
            public void onChanged(List<UserEntity> userEntities) {
                adapter.setUserList(new ArrayList<>(userEntities));
//                insertDummyData(userEntities);
            }
        });
    }

    private void insertDummyData(List<UserEntity> userEntities) {
        // If you try to run this insert task, then there will error saying, unique property
        // failed, this is because, single insert operation doesn't have OnConflict = REPLACE
        Log.d("alskjdhaj", String.format("we have %s data", userEntities.size()));

        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                AppDatabase.getDatabaseInstance(MainActivity.this)
                        .userDao()
                        .insertUser(UserEntity.dummy());
            }
        });
    }
}