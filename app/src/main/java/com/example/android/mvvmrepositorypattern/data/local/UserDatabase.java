package com.example.android.mvvmrepositorypattern.data.local;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = {UserEntity.class}, version = 2)
public abstract class UserDatabase extends RoomDatabase {

    private static UserDatabase databaseInstance;

    public abstract UserDao userDao();

    public static synchronized UserDatabase getDatabaseInstance(Context context){

        if (databaseInstance == null){
            databaseInstance = Room
                    .databaseBuilder(
                            context.getApplicationContext(),
                            UserDatabase.class,
                            "user_database")
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return databaseInstance;

    }



}
