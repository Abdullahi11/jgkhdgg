package com.example.android.mvvmrepositorypattern.ui;

import android.app.Application;

import com.example.android.mvvmrepositorypattern.data.local.UserEntity;
import com.example.android.mvvmrepositorypattern.data.remote.ResponseWrapper;
import com.example.android.mvvmrepositorypattern.data.remote.UserResponse;
import com.example.android.mvvmrepositorypattern.network.Api;
import com.example.android.mvvmrepositorypattern.network.RetrofitClient;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserViewModel extends AndroidViewModel {

    private UserRepository repository;
    private MutableLiveData<List<UserEntity>> _users;
    private ResponseWrapper<ArrayList<UserResponse>> responseWrapper = new ResponseWrapper<>();

    public UserViewModel(Application application) {
        super(application);

        initRepository(application);
        _users = new MutableLiveData<>();
        _users = repository.getUsers();
    }

    private void initRepository(Application application) {
        repository = new UserRepository(application);
    }

    MutableLiveData<List<UserEntity>> getUsers() {
        return _users;
    }

    void fetchDataFromNetworkAndStore() {

        Call<List<UserResponse>> call = getNetworkClient();
        call.enqueue(new Callback<List<UserResponse>>() {
            @Override
            public void onResponse(@NonNull Call<List<UserResponse>> call,
                                   @NonNull Response<List<UserResponse>> response) {

                if (wasRequestFailure(response)) return;
                if (isBodyEmpty(response.body())) return;
                prepareForSuccessfulResponse(response.body());
            }

            @Override
            public void onFailure(@NonNull Call<List<UserResponse>> call, @NonNull Throwable t) {
                prepareForFailureResponse();
            }
        });
    }

    private Call<List<UserResponse>> getNetworkClient() {
        Api api = RetrofitClient.getApiInstance();
        return api.getUsersList();
    }

    private boolean wasRequestFailure(Response<List<UserResponse>> response) {
        if (response.isSuccessful()) return false;
        responseWrapper.setErrorMessage("Error retrieving data from server");
        return true;
    }

    private boolean isBodyEmpty(List<UserResponse> responseBody) {
        if (responseBody != null) return false;
        responseWrapper.setErrorMessage("Empty body from server received");
        return true;
    }

    private void prepareForSuccessfulResponse(List<UserResponse> responseBody) {
        responseWrapper.setErrorMessage("Successfully retrieved data");
        responseWrapper.setResponseBody((ArrayList<UserResponse>) responseBody);
        transformResponseToEntity(responseBody);
    }

    private void transformResponseToEntity(List<UserResponse> responseList) {
        ArrayList<UserResponse> responseBody = new ArrayList<>(responseList);
        ArrayList<UserEntity> users = new ArrayList<>();

        for (UserResponse eachResponse : responseBody) {
            int userId = eachResponse.getUserId();
            String username = eachResponse.getUsername();
            users.add(new UserEntity(userId, username));
        }
        repository.insertUsers(users);
    }

    private void prepareForFailureResponse() {
        responseWrapper.setSuccessful(false);
        responseWrapper.setErrorMessage("Bad Response");
    }
}
